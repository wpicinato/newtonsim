﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BehaviourCube2 : MonoBehaviour {
	public Rigidbody rb;

	public GameObject vetorFAB;
	public GameObject vetorFBA;
	public GameObject anotherCube;
	public GameObject vetorF;
	public GameObject vetorPb;
	public GameObject vetorNb;
	public GameObject dialogues3;

	private float velocidadeB;

	public bool _isStop = true;

	void Start(){
		anotherCube = GameObject.FindGameObjectWithTag ("CubeA");

		vetorFAB = GameObject.FindGameObjectWithTag ("ForceAB");
		desactiveObject (vetorFAB);

		vetorFBA = GameObject.FindGameObjectWithTag ("ForceBA");
		desactiveObject (vetorFBA);

		vetorPb = GameObject.FindGameObjectWithTag ("ForcePb");
		desactiveObject (vetorPb);

		vetorNb = GameObject.FindGameObjectWithTag ("ForceNb");
		desactiveObject (vetorNb);

		dialogues3 = GameObject.FindGameObjectWithTag ("Panel3");
		desactiveObject (dialogues3);
	}
		
	void Update(){
		Vector3 posFAB = new Vector3 (5, 0, -4);
		Vector3 posFBA = new Vector3 (5, 0, 9);
		Vector3 posPb = new Vector3 (3, -1, 0);
		Vector3 posNb = new Vector3 (0, -4, 0);

		rb = GetComponent<Rigidbody> ();
		velocidadeB = rb.velocity.magnitude;

		vetorFAB.transform.position = rb.transform.position - posFAB;
		vetorFBA.transform.position = rb.transform.position - posFBA;
		vetorPb.transform.position = rb.transform.position - posPb;
		vetorNb.transform.position = rb.transform.position - posNb;

		if (velocidadeB > 2 && _isStop == true) {
			Time.timeScale = 0;
			activeObject (vetorFAB);
			activeObject (vetorFBA);
			activeObject (vetorPb);
			activeObject (vetorNb);
			Time.timeScale = 1;
			activeObject (dialogues3);
			_isStop = false;
		}	
		
	}

	private void activeObject(GameObject gm){
		gm.SetActive(true);
	}

	private void desactiveObject(GameObject gm){
		gm.SetActive(false);
	}

	public float GetVelocity(){
		rb = GetComponent<Rigidbody> ();
		return rb.velocity.magnitude;
	}
}
