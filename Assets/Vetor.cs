﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Vetor : MonoBehaviour {
	public Rigidbody rb;

	private float velocidadeA;

	public bool exibiu1 = false;
	//public bool exibiu2 = false;
	public bool startDialogueBox = false;

	public GameObject vetorF;
	public GameObject vetorPa;
	public GameObject vetorNa;
	public GameObject dialogues;
	public GameObject dialogues2;

	void Start(){
		vetorF = GameObject.FindGameObjectWithTag("ForceF");
		desactiveObject (vetorF);

		vetorPa = GameObject.FindGameObjectWithTag("ForcePa");
		desactiveObject (vetorPa);

		vetorNa = GameObject.FindGameObjectWithTag("ForceNa");
		desactiveObject (vetorNa);

		dialogues = GameObject.FindGameObjectWithTag ("Panel");
		desactiveObject (dialogues);

		dialogues2 = GameObject.FindGameObjectWithTag ("Panel2");
		desactiveObject (dialogues2);
	}

	// Update is called once per frame
	void Update () {
		Vector3 posF = new Vector3 (0, -2, 10);
		Vector3 posPa = new Vector3 (3, -1, 0);
		Vector3 posNa = new Vector3 (0, -4, 0);

		rb = GetComponent<Rigidbody> ();
		velocidadeA = rb.velocity.magnitude;

		vetorF.transform.position = rb.transform.position - posF;
		vetorPa.transform.position = rb.transform.position - posPa;
		vetorNa.transform.position = rb.transform.position - posNa;

		if (velocidadeA == 0 && startDialogueBox == false) {
			activeObject (dialogues);
		}

		else {
			desactiveObject (dialogues);
		}

		if (velocidadeA > 4 && exibiu1 == false){
			Time.timeScale = 0;
			activeObject (vetorF);
			activeObject (vetorPa);
			activeObject (vetorNa);
			Time.timeScale = 1;
			activeObject(dialogues2);
			//startDialogueBox = true;
		}

		if(Input.GetKeyDown("f")){
			if (dialogues.activeSelf == true) {
				desactiveObject (dialogues);
				startDialogueBox = true;
			}

			if (dialogues2.activeSelf == true) {
				desactiveObject (dialogues2);
				exibiu1 = true;
			}
		}
	}

	public void activeObject(GameObject gm){
		gm.SetActive(true);
	}

	public void desactiveObject(GameObject gm){
		gm.SetActive (false);
	}
		
}
